# WOLF
---
A Wolfenstein-3D clone created for [DOS Games June 2023 Jam](https://itch.io/jam/dos-games-june-2023-jam).

Created using FreeDOS, C89 and uses VGA Graphics mode.
