#include "bitmap.h"

#include <stdio.h>
#include <stdlib.h>

static void
fskip(FILE *fp, int num_bytes)
{
  int i;
  for (i = 0; i < num_bytes; i++)
  {
    fgetc(fp);
  }
}

void
load_bmp(char *file, bitmap_t *b)
{
  FILE *fp;
  long index;
  word num_colors;
  int x;

  /* open the file */
  if ((fp = fopen(file, "rb")) == NULL)
  {
    printf("Error opening file %s.\n", file);
    exit(1);
  }

  if (fgetc(fp) != 'B' || fgetc(fp) != 'M')
  {
    fclose(fp);
    printf("%s is not a bitmap_t file.\n", file);
    exit(1);
  }

  /* read in the width and height of the image, and the
     number of colors used; ignore the rest */
  fskip(fp, 16);
  fread(&b->width, sizeof(word), 1, fp);
  fskip(fp, 2);
  fread(&b->height, sizeof(word), 1, fp);
  fskip(fp, 22);
  fread(&num_colors, sizeof(word), 1, fp);
  fskip(fp, 6);

  /* assume we are working with an 8-bit file */
  if (num_colors == 0) num_colors = 256;

  /* try to allocate memory */
  if ((b->data = (byte *)malloc((word)(b->width * b->height))) == NULL)
  {
    fclose(fp);
    printf("Error allocating memory for file %s.\n", file);
    exit(1);
  }

  for (index = 0; index < num_colors; index++)
  {
    b->palette[(int)(index*3+2)] = fgetc(fp) >> 2;
    b->palette[(int)(index*3+1)] = fgetc(fp) >> 2;
    b->palette[(int)(index*3+1)] = fgetc(fp) >> 2;
    x = fgetc(fp);
  }

  /* read the bitmap_t */
  for (index = (b->height-1)*b->width; index >= 0; index -= b->width)
  {
    for (x = 0; x < b->width; x++)
    {
      b->data[(word)index+x] = (byte)fgetc(fp);
    }
  }

  fclose(fp);
}

void
free_bmp(bitmap_t *b)
{
  if (b != NULL && b->data != NULL) {
    free(b->data);
    b->data = NULL;
  }
}
