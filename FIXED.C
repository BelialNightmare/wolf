#include "fixed.h"

float
fixtof(fixed_t fix, long frac)
{
  return (float)fix / (1 << frac);
}

fixed_t
ftofix(float f, long frac)
{
  return (fixed_t)(f * (1 << frac));
}

fixed_t
fixmul(fixed_t f1, fixed_t f2, long frac)
{
  return (f1 * f2) >> frac;
}
