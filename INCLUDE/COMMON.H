#ifndef __H_COMMON__
#define __H_COMMON__

#ifdef USE_COMMON_INCLUDES
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <mem.h>
#include <i86.h>
#include <math.h>
#endif

#define VIDEO_INT          0x10 /* BIOS video interrupt */
#define SET_MODE           0x00 /* BIOS func to set the video mode */
#define VGA_256_COLOR_MODE 0x13 /* use to set 256-color mode. */
#define TEXT_MODE          0x03 /* use to set 80x25 text mode. */

#define PALETTE_INDEX      0x03c8
#define PALETTE_DATA       0x03c9
#define INPUT_STATUS       0x03da
#define VRETRACE           0x08

#define MOUSE_INT          0x33
#define MOUSE_RESET        0x00
#define MOUSE_GETPRESS     0x05
#define MOUSE_GETRELEASE   0x06
#define MOUSE_GETMOTION    0x0B
#define LEFT_BUTTON        0x00
#define RIGHT_BUTTON       0x01
#define MIDDLE_BUTTON      0x02

#define MOUSE_WIDTH        24
#define MOUSE_HEIGHT       24
#define MOUSE_SIZE         (MOUSE_WIDTH * MOUSE_HEIGHT)

#define BUTTON_WIDTH       48
#define BUTTON_HEIGHT      24
#define BUTTON_SIZE        (BUTTON_WIDTH * BUTTON_HEIGHT)
#define BUTTON_BITMAPS     3
#define STATE_NORM         0
#define STATE_ACTIVE       1
#define STATE_PRESSED      2
#define STATE_WAITING      3

#define NUM_BUTTONS        2
#define NUM_MOUSEBITMAPS   9

#define SCREEN_WIDTH       320
#define HALF_WIDTH         160
#define SCREEN_HEIGHT      200
#define HALF_HEIGHT        100
#define NUM_COLORS         256

#define WHOLE_PART         16
#define FRAC_PART          16

#define sgn(x) ((x < 0)?-1:((x>0)?1:0))

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned char byte;
typedef unsigned short word;
typedef unsigned long dword;
typedef short sword; /* signed xword */

#ifdef __cplusplus
}
#endif

#endif /* __H_COMMON__ */
